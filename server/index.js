const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const random = require("lodash.random");

const app = express();
const port = 5000;

let tokens = [];
let tableTokens = [];

app.use(bodyParser.json());
app.use(
	bodyParser.urlencoded({
		extended: true
	})
);
app.use(cors());

app.post("/add-token", (req, res) => {
	const token = req.body;

	tokens = [...tokens, token];
	res.sendStatus(200);
});

app.post("/add-tokens", (req, res) => {
	const new_tokens = req.body;
	
	tokens = tokens.concat(new_tokens);
	res.sendStatus(200);
});

app.post("/remove-token", (req, res) => {
	const token = req.body;
	//usuwanie tokenu z listy
	var toKeep = tokens.filter(
		function(value){ 
			return !( (value.color == token.color) && (value.rank == token.rank) )
		}
	);

	var toRemove = tokens.filter(
		function(value){ 
			return ( (value.color == token.color) && (value.rank == token.rank) )
		}
	);
	toRemove.splice(0,1)
	tokens = toKeep.concat(toRemove)	
	res.sendStatus(200);
});

app.post("/draw-token", (req, res) => {
	if (!tokens.length) {
		return res.sendStatus(200);
	}
	const randomIndex = random(0, tokens.length - 1);
	const selectedToken = tokens[randomIndex];
	tokens.splice(randomIndex, 1);
	tableTokens = [...tableTokens, selectedToken];
	res.sendStatus(200);
});

app.post("/reset-pool", (req, res) => {
	tokens = [];
	tableTokens = [];
	res.sendStatus(200);
});

app.get("/pool", (req, res) => {
	res.send({ tokens });
});

app.get("/table-tokens", (req, res) => {
	res.send({ tableTokens });
});

app.listen(port, () => console.log(`App listening on port ${port}!`));
