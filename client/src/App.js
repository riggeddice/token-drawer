import React, { useState, useEffect } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';

import TokenDefinitionsPage from './TokenDefinitionsPage';
import TokenPoolPage from './TokenPoolPage';
import CurrentTablePage from './CurrentTablePage';

const baseUrl = 'http://51.75.253.114:5000';

function App() {
  const [tokens, setTokens] = useState([]);
  const [currentTableTokens, setCurrentLabelTokens] = useState([]);
  const [shouldUpdate, setShouldUpdate] = useState(true);

  const handleDrawButton = () =>
    fetch(`${baseUrl}/draw-token`, {
      method: 'POST',
    }).then(() => setShouldUpdate(true));

  const handleResetButton = () =>
    fetch(`${baseUrl}/reset-pool`, {
      method: 'POST',
    }).then(() => setShouldUpdate(true));


    const handleTypicalButton = () =>
    fetch(`${baseUrl}/add-tokens`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify([
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'success', color: 'none' },
        { rank: 'success', color: 'none' },
        { rank: 'success', color: 'none' },
        { rank: 'success', color: 'none' },
        { rank: 'success', color: 'none' },
        { rank: 'success', color: 'none' },
        { rank: 'success', color: 'none' },
        { rank: 'success', color: 'none' },
        { rank: 'success', color: 'none' }
    ]),
    }).then(() => setShouldUpdate(true));


    const handleHardButton = () =>
    fetch(`${baseUrl}/add-tokens`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify([
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'success', color: 'none' },
        { rank: 'success', color: 'none' },
        { rank: 'success', color: 'none' },
        { rank: 'success', color: 'none' },
        { rank: 'success', color: 'none' },
        { rank: 'success', color: 'none' }
    ]),
    }).then(() => setShouldUpdate(true));

    const handleExtremeButton = () =>
    fetch(`${baseUrl}/add-tokens`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify([
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'success', color: 'none' },
        { rank: 'success', color: 'none' },
        { rank: 'success', color: 'none' }
    ]),
    }).then(() => setShouldUpdate(true));


    const handleHeroicButton = () =>
    fetch(`${baseUrl}/add-tokens`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify([
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' },
        { rank: 'failure', color: 'none' }
    ]),
    }).then(() => setShouldUpdate(true));

  const updateTokensPool = token =>
    fetch(`${baseUrl}/add-token`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(token),
    }).then(() => setShouldUpdate(true));

    const removeTokenFromPool = token =>
    fetch(`${baseUrl}/remove-token`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(token),
    }).then(() => setShouldUpdate(true));

  useEffect(() => {
    if (shouldUpdate) {
      fetch(`${baseUrl}/pool`)
        .then(responose => responose.json())
        .then(({ tokens: newTokens }) => setTokens(newTokens))
        .then(() => setShouldUpdate(false));
    }
  }, [shouldUpdate, tokens]);

  useEffect(() => {
    if (shouldUpdate) {
      fetch(`${baseUrl}/table-tokens`)
        .then(responose => responose.json())
        .then(({ tableTokens }) => setCurrentLabelTokens(tableTokens))
        .then(() => setShouldUpdate(false));
    }
  }, [shouldUpdate]);

  useEffect(() => {
    const interval = setInterval(async () => {
      const { tableTokens } = await fetch(
        `${baseUrl}/table-tokens`,
      ).then(responsne => responsne.json());
      const { tokens } = await fetch(`${baseUrl}/pool`).then(responsne =>
        responsne.json(),
      );

      setCurrentLabelTokens(tableTokens);
      setTokens(tokens);
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  return (
    <div className="App">
      <div className="row custom-border">
        <div className="col-6 border-right">
          <TokenDefinitionsPage updateTokensPool={updateTokensPool} />
        <div className="row custom-border">
          <div className="col-5">
          <button
            type="button"
            className="btn btn-primary btn-lg button"
            onClick={handleTypicalButton}
          >
            Typowy
          </button>
          </div>
          <div className="col-5">
          <button
            type="button"
            className="btn btn-primary btn-lg button"
            onClick={handleHardButton}
          >
            Trudny
          </button>
          </div>
        </div>  
        <div className="row custom-border">
        <div className="col-5">
          <button
            type="button"
            className="btn btn-primary btn-lg button"
            onClick={handleExtremeButton}
          >
            Ekstremalny
          </button>
          </div>
          <div className="col-5">
          <button
            type="button"
            className="btn btn-primary btn-lg button"
            onClick={handleHeroicButton}
          >
            Heroiczny
          </button>
          </div>
        </div>  
        </div>
        <div className="col-5">
          <TokenPoolPage tokens={tokens} updateTokensPool={removeTokenFromPool} />
        </div>
      </div>

      <div className="row">
        <div className="current-table-container">
          <CurrentTablePage currentTableTokens={currentTableTokens} />
        </div>
      </div>

      <div className="row">
        <div className="col-6">
          <button
            type="button"
            className="btn btn-primary btn-lg button"
            onClick={handleDrawButton}
          >
            Draw
          </button>
        </div>
        <div className="col-6">
          <button
            type="button"
            className="btn btn-secondary btn-lg button"
            onClick={handleResetButton}
          >
            Reset
          </button>
        </div>
      </div>
    </div>
  );
}

export default App;
