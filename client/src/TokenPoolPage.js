import React from 'react';
import Token from './Token';

const TokenPoolPage = ({ tokens, updateTokensPool }) => (
  <div className="token-pool-container">
    {tokens.map(({ rank, color }) => (
      <Token rank={rank} color={color} updateTokensPool={updateTokensPool} />
    ))}
  </div>
);

export default TokenPoolPage;
