import React from 'react';
import Token from './Token';

const CurrentTablePage = ({ currentTableTokens }) => (
  <div className="token-pool-container">
    {currentTableTokens.map(({ rank, color }) => (
      <Token rank={rank} color={color} />
    ))}
  </div>
);

export default CurrentTablePage;
