import React from 'react';

const tokenColorsDictionary = {
  none: '#e0e0d1',
  blue: '#5277ff',
  violet: '#8e52ff',
  green: '#38f000',
  yellow: '#ffeb52',
  red: '#ff2a12',
};

const tokenRanksDictionary = {
  success: '✔',
  failure: '⤫',
  entropy: '⊚',
};

const Token = ({ rank, color, updateTokensPool }) => (
  <div
    className={updateTokensPool ? 'definition-token' : 'pool-token'}
    style={{
      backgroundColor: tokenColorsDictionary[color],
    }}

    onClick={event => updateTokensPool && updateTokensPool({ rank, color })}
    disabled={!updateTokensPool}
  >
    <div
      style={{
        transform: 'translateY(8px)',
      }}
    >
      {tokenRanksDictionary[rank]}
    </div>
  </div>
);

export default Token;
