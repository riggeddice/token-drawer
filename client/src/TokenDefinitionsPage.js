import React from 'react';
import Token from './Token';

const tokenColors = ['none', 'blue', 'violet', 'yellow', 'red'];
const tokenRanks = ['success', 'failure', 'entropy'];

const tokensDefinition = tokenRanks.reduce((acc, rank) => {
  for (const color of tokenColors) {
    acc = [
      ...acc,
      {
        rank,
        color,
      },
    ];
  }

  return acc;
}, []);

const TokenDefinitionsPage = ({ updateTokensPool }) => (
  <div className="token-definition-page">
    {tokensDefinition.map(({ rank, color }) => (
      <Token rank={rank} color={color} updateTokensPool={updateTokensPool} />
    ))}
  </div>
);

export default TokenDefinitionsPage;
